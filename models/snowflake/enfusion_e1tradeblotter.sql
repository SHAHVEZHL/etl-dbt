with source_data as (
    select
        {{ json_extract_scalar('_airbyte_data', ['Trade Date'])}} as {{ adapter.quote('trade_date')}},
        {{ json_extract_scalar('_airbyte_data', ['BB Yellow Key'])}} as {{ adapter.quote('bb_yellow_key')}},
        {{ json_extract_scalar('_airbyte_data', ['Description'])}} as {{ adapter.quote('description')}},
        {{ json_extract_scalar('_airbyte_data', ['Txn Type'])}} as {{ adapter.quote('txn_type')}},
        {{ json_extract_scalar('_airbyte_data', ['Quantity'])}} as {{ adapter.quote('quantity')}},
        {{ json_extract_scalar('_airbyte_data', ['Price'])}} as {{ adapter.quote('price')}},
        {{ json_extract_scalar('_airbyte_data', ['Gross Amount'])}} as {{ adapter.quote('gross_amount')}},
        {{ json_extract_scalar('_airbyte_data', ['Commissions'])}} as {{ adapter.quote('commissions')}},
        {{ json_extract_scalar('_airbyte_data', ['Fees'])}} as {{ adapter.quote('fees')}},
        {{ json_extract_scalar('_airbyte_data', ['Net Amount'])}} as {{ adapter.quote('net_amount')}},
        {{ json_extract_scalar('_airbyte_data', ['Trade Currency'])}} as {{ adapter.quote('trade_currency')}},
        {{ json_extract_scalar('_airbyte_data', ['Fund'])}} as {{ adapter.quote('fund')}},
        {{ json_extract_scalar('_airbyte_data', ['Book'])}} as {{ adapter.quote('book')}},
        {{ json_extract_scalar('_airbyte_data', ['Account'])}} as {{ adapter.quote('account')}},
        {{ json_extract_scalar('_airbyte_data', ['Counterparty'])}} as {{ adapter.quote('counterparty')}},
        {{ json_extract_scalar('_airbyte_data', ['Trade Id'])}} as {{ adapter.quote('trade_id')}},
        {{ json_extract_scalar('_airbyte_data', ['Trade Canceled'])}} as {{ adapter.quote('trade_canceled')}},
        {{ json_extract_scalar('_airbyte_data', ['TradeLastModified'])}} as {{ adapter.quote('tradelastmodified')}}
    from {{ source('detl', '_airbyte_raw_enfusion_e1tradeblotter')}}
),
final as (
    select
        cast({{ adapter.quote('trade_date') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('trade_date') }},
        cast({{ adapter.quote('bb_yellow_key') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('bb_yellow_key') }},
        cast({{ adapter.quote('description') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('description') }},
        cast({{ adapter.quote('txn_type') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('txn_type') }},
        cast({{ adapter.quote('quantity') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('quantity') }},
        cast({{ adapter.quote('price') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('price') }},
        cast({{ adapter.quote('gross_amount') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('gross_amount') }},
        cast({{ adapter.quote('commissions') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('commissions') }},
        cast({{ adapter.quote('fees') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('fees') }},
        cast({{ adapter.quote('net_amount') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('net_amount') }},
        cast({{ adapter.quote('trade_currency') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('trade_currency') }},
        cast({{ adapter.quote('fund') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('fund') }},
        cast({{ adapter.quote('book') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('book') }},
        cast({{ adapter.quote('account') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('account') }},
        cast({{ adapter.quote('counterparty') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('counterparty') }},
        cast({{ adapter.quote('trade_id') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('trade_id') }},
        cast({{ adapter.quote('trade_canceled') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('trade_canceled') }},
        cast({{ adapter.quote('tradelastmodified') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('tradelastmodified') }}
    from source_data
)
select * from final
