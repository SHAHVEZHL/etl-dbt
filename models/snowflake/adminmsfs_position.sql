with source_data as (
    select
        {{ json_extract_scalar('_airbyte_data', ['CUSIP'])}} as {{ adapter.quote('cusip')}},
        {{ json_extract_scalar('_airbyte_data', ['UNDERLYING CUSIP'])}} as {{ adapter.quote('underlying_cusip')}},
        {{ json_extract_scalar('_airbyte_data', ['SYMBOL'])}} as {{ adapter.quote('symbol')}},
        {{ json_extract_scalar('_airbyte_data', ['SECURITY DESCRIPTION'])}} as {{ adapter.quote('security_description')}},
        {{ json_extract_scalar('_airbyte_data', ['CURRENCY CODE (ISSUE)'])}} as {{ adapter.quote('currency_code_issue')}},
        {{ json_extract_scalar('_airbyte_data', ['CURRENCY DESCRIPTION (ISSUE)'])}} as {{ adapter.quote('currency_description_issue')}},
        {{ json_extract_scalar('_airbyte_data', ['QUANTITY AT THE BEGINNING OF THE PERIOD'])}} as {{ adapter.quote('quantity_at_the_beginning_of_the_period')}},
        {{ json_extract_scalar('_airbyte_data', ['COST – NET (BASE) AT BEGINNING OF PERIOD'])}} as {{ adapter.quote('cost_net_base_at_beginning_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['PRICE (ISSUE) AT BEGINNING OF PERIOD'])}} as {{ adapter.quote('price_issue_at_beginning_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['FX RATE AT BEGINNING OF PERIOD'])}} as {{ adapter.quote('fx_rate_at_beginning_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['PRICE (ISSUE) AT END OF PERIOD'])}} as {{ adapter.quote('price_issue_at_end_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['FX RATE AT END OF PERIOD'])}} as {{ adapter.quote('fx_rate_at_end_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['MARKET VALUE – NET (BASE) AT BEGINNING OF PERIOD'])}} as {{ adapter.quote('market_value_net_base_at_beginning_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['UNREALIZED GAIN/LOSS – MARKET (BASE) AT THE BEGINNING OF PERIOD'])}} as {{ adapter.quote('unrealized_gain_loss_market_base_at_the_beginning_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['UNREALIZED GAIN/LOSS – FX (BASE) AT THE BEGINNING OF PERIOD'])}} as {{ adapter.quote('unrealized_gain_loss_fx_base_at_the_beginning_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['QUANTITY AT THE  END OF THE PERIOD'])}} as {{ adapter.quote('quantity_at_the_end_of_the_period')}},
        {{ json_extract_scalar('_airbyte_data', ['COST – NET (BASE) AT THE END OF THE PERIOD'])}} as {{ adapter.quote('cost_net_base_at_the_end_of_the_period')}},
        {{ json_extract_scalar('_airbyte_data', ['MARKET VALUE – NET (BASE) AT THE END OF THE PERIOD'])}} as {{ adapter.quote('market_value_net_base_at_the_end_of_the_period')}},
        {{ json_extract_scalar('_airbyte_data', ['UNREALIZED GAIN/LOSS – MARKET (BASE) AT THE END OF PERIOD'])}} as {{ adapter.quote('unrealized_gain_loss_market_base_at_the_end_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['UNREALIZED GAIN/LOSS – FX (BASE) AT THE END OF PERIOD'])}} as {{ adapter.quote('unrealized_gain_loss_fx_base_at_the_end_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['COST (BASE) CHANGE FOR THE PERIOD'])}} as {{ adapter.quote('cost_base_change_for_the_period')}},
        {{ json_extract_scalar('_airbyte_data', ['CHANGE IN UNREALIZED GAIN/LOSS - MARKET'])}} as {{ adapter.quote('change_in_unrealized_gain_loss_market')}},
        {{ json_extract_scalar('_airbyte_data', ['CHANGE IN UNREALIZED GAIN/LOSS - FX'])}} as {{ adapter.quote('change_in_unrealized_gain_loss_fx')}},
        {{ json_extract_scalar('_airbyte_data', ['REALIZED GAIN/LOSS BASE - MARKET'])}} as {{ adapter.quote('realized_gain_loss_base_market')}},
        {{ json_extract_scalar('_airbyte_data', ['REALIZED GAIN/LOSS BASE - FX'])}} as {{ adapter.quote('realized_gain_loss_base_fx')}},
        {{ json_extract_scalar('_airbyte_data', ['DIVIDEND OR INTEREST INCOME'])}} as {{ adapter.quote('dividend_or_interest_income')}},
        {{ json_extract_scalar('_airbyte_data', ['TOTAL INCOME'])}} as {{ adapter.quote('total_income')}},
        {{ json_extract_scalar('_airbyte_data', ['PORTFOLIO ID'])}} as {{ adapter.quote('portfolio_id')}},
        {{ json_extract_scalar('_airbyte_data', ['MONEY MANAGER CODE'])}} as {{ adapter.quote('money_manager_code')}},
        {{ json_extract_scalar('_airbyte_data', ['CUSTODIAN CODE'])}} as {{ adapter.quote('custodian_code')}},
        {{ json_extract_scalar('_airbyte_data', ['LONG/SHORT/NET'])}} as {{ adapter.quote('long_short_net')}},
        {{ json_extract_scalar('_airbyte_data', ['SEDOL'])}} as {{ adapter.quote('sedol')}},
        {{ json_extract_scalar('_airbyte_data', ['ASSET CLASS LEVEL 1'])}} as {{ adapter.quote('asset_class_level_1')}},
        {{ json_extract_scalar('_airbyte_data', ['ASSET CLASS CODE'])}} as {{ adapter.quote('asset_class_code')}},
        {{ json_extract_scalar('_airbyte_data', ['CONTRACT ID'])}} as {{ adapter.quote('contract_id')}},
        {{ json_extract_scalar('_airbyte_data', ['COST – GROSS (BASE) AT BEGINNING OF PERIOD'])}} as {{ adapter.quote('cost_gross_base_at_beginning_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['COST – GROSS (BASE) AT THE END OF THE PERIOD'])}} as {{ adapter.quote('cost_gross_base_at_the_end_of_the_period')}},
        {{ json_extract_scalar('_airbyte_data', ['MARKET VALUE – GROSS (BASE) AT BEGINNING OF PERIOD'])}} as {{ adapter.quote('market_value_gross_base_at_beginning_of_period')}},
        {{ json_extract_scalar('_airbyte_data', ['MARKET VALUE – GROSS (BASE) AT THE END OF THE PERIOD'])}} as {{ adapter.quote('market_value_gross_base_at_the_end_of_the_period')}},
        {{ json_extract_scalar('_airbyte_data', ['NOTIONAL FLAG'])}} as {{ adapter.quote('notional_flag')}},
        {{ json_extract_scalar('_airbyte_data', ['POSITION TYPE'])}} as {{ adapter.quote('position_type')}},
        {{ json_extract_scalar('_airbyte_data', ['POSITION TYPE CODE'])}} as {{ adapter.quote('position_type_code')}},
        {{ json_extract_scalar('_airbyte_data', ['ISIN'])}} as {{ adapter.quote('isin')}},
        {{ json_extract_scalar('_airbyte_data', ['QUICK ID'])}} as {{ adapter.quote('quick_id')}},
        {{ json_extract_scalar('_airbyte_data', ['RIC'])}} as {{ adapter.quote('ric')}},
        {{ json_extract_scalar('_airbyte_data', ['COUNTRY CODE (ISSUE)'])}} as {{ adapter.quote('country_code_issue')}},
        {{ json_extract_scalar('_airbyte_data', ['COUNTRY CODE (ISSUER)'])}} as {{ adapter.quote('country_code_issuer')}},
        {{ json_extract_scalar('_airbyte_data', ['Code1256'])}} as {{ adapter.quote('code1256')}},
        {{ json_extract_scalar('_airbyte_data', ['Bullet Swap Indicator'])}} as {{ adapter.quote('bullet_swap_indicator')}},
        {{ json_extract_scalar('_airbyte_data', ['Cash Flow Description'])}} as {{ adapter.quote('cash_flow_description')}},
        {{ json_extract_scalar('_airbyte_data', ['Announcement_Id'])}} as {{ adapter.quote('announcement_id')}},
        {{ json_extract_scalar('_airbyte_data', ['FILE DATE'])}} as {{ adapter.quote('file_date')}}
    from {{ source('detl', '_airbyte_raw_adminmsfs_position')}}
),
final as (
    select
        cast({{ adapter.quote('cusip') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('cusip') }},
        cast({{ adapter.quote('underlying_cusip') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('underlying_cusip') }},
        cast({{ adapter.quote('symbol') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('symbol') }},
        cast({{ adapter.quote('security_description') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('security_description') }},
        cast({{ adapter.quote('currency_code_issue') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('currency_code_issue') }},
        cast({{ adapter.quote('currency_description_issue') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('currency_description_issue') }},
        cast({{ adapter.quote('quantity_at_the_beginning_of_the_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('quantity_at_the_beginning_of_the_period') }},
        cast({{ adapter.quote('cost_net_base_at_beginning_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('cost_net_base_at_beginning_of_period') }},
        cast({{ adapter.quote('price_issue_at_beginning_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('price_issue_at_beginning_of_period') }},
        cast({{ adapter.quote('fx_rate_at_beginning_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('fx_rate_at_beginning_of_period') }},
        cast({{ adapter.quote('price_issue_at_end_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('price_issue_at_end_of_period') }},
        cast({{ adapter.quote('fx_rate_at_end_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('fx_rate_at_end_of_period') }},
        cast({{ adapter.quote('market_value_net_base_at_beginning_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('market_value_net_base_at_beginning_of_period') }},
        cast({{ adapter.quote('unrealized_gain_loss_market_base_at_the_beginning_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('unrealized_gain_loss_market_base_at_the_beginning_of_period') }},
        cast({{ adapter.quote('unrealized_gain_loss_fx_base_at_the_beginning_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('unrealized_gain_loss_fx_base_at_the_beginning_of_period') }},
        cast({{ adapter.quote('quantity_at_the_end_of_the_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('quantity_at_the_end_of_the_period') }},
        cast({{ adapter.quote('cost_net_base_at_the_end_of_the_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('cost_net_base_at_the_end_of_the_period') }},
        cast({{ adapter.quote('market_value_net_base_at_the_end_of_the_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('market_value_net_base_at_the_end_of_the_period') }},
        cast({{ adapter.quote('unrealized_gain_loss_market_base_at_the_end_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('unrealized_gain_loss_market_base_at_the_end_of_period') }},
        cast({{ adapter.quote('unrealized_gain_loss_fx_base_at_the_end_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('unrealized_gain_loss_fx_base_at_the_end_of_period') }},
        cast({{ adapter.quote('cost_base_change_for_the_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('cost_base_change_for_the_period') }},
        cast({{ adapter.quote('change_in_unrealized_gain_loss_market') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('change_in_unrealized_gain_loss_market') }},
        cast({{ adapter.quote('change_in_unrealized_gain_loss_fx') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('change_in_unrealized_gain_loss_fx') }},
        cast({{ adapter.quote('realized_gain_loss_base_market') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('realized_gain_loss_base_market') }},
        cast({{ adapter.quote('realized_gain_loss_base_fx') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('realized_gain_loss_base_fx') }},
        cast({{ adapter.quote('dividend_or_interest_income') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('dividend_or_interest_income') }},
        cast({{ adapter.quote('total_income') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('total_income') }},
        cast({{ adapter.quote('portfolio_id') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('portfolio_id') }},
        cast({{ adapter.quote('money_manager_code') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('money_manager_code') }},
        cast({{ adapter.quote('custodian_code') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('custodian_code') }},
        cast({{ adapter.quote('long_short_net') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('long_short_net') }},
        cast({{ adapter.quote('sedol') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('sedol') }},
        cast({{ adapter.quote('asset_class_level_1') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('asset_class_level_1') }},
        cast({{ adapter.quote('asset_class_code') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('asset_class_code') }},
        cast({{ adapter.quote('contract_id') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('contract_id') }},
        cast({{ adapter.quote('cost_gross_base_at_beginning_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('cost_gross_base_at_beginning_of_period') }},
        cast({{ adapter.quote('cost_gross_base_at_the_end_of_the_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('cost_gross_base_at_the_end_of_the_period') }},
        cast({{ adapter.quote('market_value_gross_base_at_beginning_of_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('market_value_gross_base_at_beginning_of_period') }},
        cast({{ adapter.quote('market_value_gross_base_at_the_end_of_the_period') }} as {{ dbt_utils.type_float()}}) as {{ adapter.quote('market_value_gross_base_at_the_end_of_the_period') }},
        cast({{ adapter.quote('notional_flag') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('notional_flag') }},
        cast({{ adapter.quote('position_type') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('position_type') }},
        cast({{ adapter.quote('position_type_code') }} as {{ dbt_utils.type_int()}}) as {{ adapter.quote('position_type_code') }},
        cast({{ adapter.quote('isin') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('isin') }},
        cast({{ adapter.quote('quick_id') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('quick_id') }},
        cast({{ adapter.quote('ric') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('ric') }},
        cast({{ adapter.quote('country_code_issue') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('country_code_issue') }},
        cast({{ adapter.quote('country_code_issuer') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('country_code_issuer') }},
        cast({{ adapter.quote('code1256') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('code1256') }},
        cast({{ adapter.quote('bullet_swap_indicator') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('bullet_swap_indicator') }},
        cast({{ adapter.quote('cash_flow_description') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('cash_flow_description') }},
        cast({{ adapter.quote('announcement_id') }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('announcement_id') }},
        cast({{ adapter.quote('file_date') }} as {{ dbt_utils.type_timestamp()}}) as {{ adapter.quote('file_date') }},
	cast({{ -1  }} as {{ dbt_utils.type_int()}}) as {{ adapter.quote('secid') }},
	cast({{ 'RAW' }} as {{ dbt_utils.type_string()}}) as {{ adapter.quote('etl_status') }}
		

from source_data
)
select * from final
